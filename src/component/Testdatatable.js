import React, { Component } from 'react'
import './datatablenet.css'
import { Modal, Button } from 'antd';
import { Input } from 'antd';
import { DatePicker, InputNumber, Upload, message, Radio, Row, Col, TimePicker, } from 'antd';
import "react-datepicker/dist/react-datepicker.css";
import moment from 'moment';
import { get, post } from '../service/service';
import swal from 'sweetalert';

var $ = require('jquery');
$.DataTable = require('datatables.net')
const dataSet = [
    ["Tiger Nixon", "System Architect", "Edinburgh", "5421", "2011/04/25", "$320,800",],
    ["Garrett Winters", "Accountant", "Tokyo", "8422", "2011/07/25", "$170,750"],
    ["Ashton Cox", "Junior Technical Author", "San Francisco", "1562", "2009/01/12", "$86,000"],
    ["Cedric Kelly", "Senior Javascript Developer", "Edinburgh", "6224", "2012/03/29", "$433,060"],
    ["Airi Satou", "Accountant", "Tokyo", "5407", "2008/11/28", "$162,700"],
    ["Brielle Williamson", "Integration Specialist", "New York", "4804", "2012/12/02", "$372,000"],
    ["Herrod Chandler", "Sales Assistant", "San Francisco", "9608", "2012/08/06", "$137,500"],
    ["Rhona Davidson", "Integration Specialist", "Tokyo", "6200", "2010/10/14", "$327,900"],
    ["Colleen Hurst", "Javascript Developer", "San Francisco", "2360", "2009/09/15", "$205,500"],
    ["Sonya Frost", "Software Engineer", "Edinburgh", "1667", "2008/12/13", "$103,600"],


];



class testdatatable extends Component {
    constructor() {
        super();
        this.state = {
            username: null,
            password: null,
            data: [],
            moment: moment(),
            startValue: null,
            endValue: null,
            endOpen: false,
            value: 1,
            lea_day: "FullDay",
            leaName_type: null,
            date_start: moment().format("DD/MM/YYYY"),
            date_end: moment().format("DD/MM/YYYY"),
            time_start: moment().format("HH:mm:ss"),
            time_end: moment().format("HH:mm:ss"),
            lea_reason: null,
            radio: null,
            lea_reason: null,
            leaName_day: null,
            lea_day: null,
            date_start: null,
            date_end: null,
            add_empID:null,
            Empid: null,
            leaveID: null,
        };
    }
    // componentDidMount = async () => {
    //     try {
    //         let res = await get('/show_user')
    //         this.setState({
    //             data: res.result
    //         }, () => console.log(this.state.data)
    //         )
    //     } catch (error) {
    //         alert(error)
    //     }
    // }

    _approverequest = () => {
        let  add_empID = this.state.Empid;
        let  leaID  = this.state.leaveID;
              

        swal({
            title: "Are You Sure To Approve?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willapprove) => {
                if (willapprove) {
                    this._approveyet(add_empID, leaID)

                } else {
                    swal("Fail");
                }
            });
    }
    _approveyet = async (add_empID, leaID) => {
        

        try {

            let res = await post('/update_leave_request',{ add_empID, leaveID:leaID } )
            if (res.success) {
                swal("Complete", {
                    icon: "success",
                });
                window.location.href = "/leaverequestadmin"
            } else {
                console.log("ผิดพลาด")
            }


        } catch (err) {
            console.log(err)
        }
    }
    _rejectrequest = () => {
        let  add_empID = this.state.Empid   ;
        let  leaID  = this.state.leaveID;
              

        swal({
            title: "Are You Sure To Reject?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willreject) => {
                if (willreject) {
                    this._rejectyet(add_empID, leaID)

                } else {
                    swal("Fail");
                }
            });
    }
    _rejectyet = async (add_empID, leaID) => {
        

        try {

            let res = await post('/update_leave_request_1',{ add_empID, leaveID:leaID } )
            if (res.success) {
                swal("Complete", {
                    icon: "success",
                });
                window.location.href = "/leaverequestadmin"
            } else {
                console.log("ผิดพลาด")
            }


        } catch (err) {
            console.log(err)
        }
    }

    componentWillMount() {
        this.setState({
            ...this.props


        })
        console.log("ข้อมูลที่มา", this.props)
        console.log("leaName_day", JSON.stringify(this.props.leaName_day))
        

    }
    onChange_radio = (event) => {
        console.log('radio checked', event.target.value);
        this.setState({
            radio: event.target.value,
        });
    }

    render() {
        const { leaName_type, lea_day, radio, lea_reason, leaName_day, date_start, date_end,Empid,leaveID } = this.state
        const RadioGroup = Radio.Group;
        const { TextArea } = Input;
        const dateFormat = 'YYYY/MM/DD';
        return (


            <div style={{ background: "#ffffff" }}>
                <div style={{ width: '100%', textAlign: '-webkit-center', fontSize: 20 }}>
                    <div style={{ color: '#00b8e6' }}>
                        {this.state.rol_name}

                    </div>
                    <div >
                        {this.state.full_name}

                    </div>


                    <div style={{ fontSize: 20, color: '#00b8e6', borderBottomStyle: 'ridge', width: 180, marginTop: 30, textAlign: '-webkit-center' }}>
                        LeaveRequest
                    </div>

                    <div style={{ width: 400, marginTop: 40 }}>


                        <div style={{ width: 400, marginTop: 40 }} >
                            <Input placeholder="Basic usage" value={leaName_type} />
                            <Input placeholder="Basic usage" value={leaName_day} style={{ marginTop: 20 }} />
                            {/* <div class="input-group mb-3"  value={leaName_day}>

                                <select class="custom-select" id="inputGroupSelect01"  value={leaName_day}>
                                    <option  value={leaName_day}></option>
                                    <option  value={leaName_day}></option>

                                </select>
                            </div> */}
                        </div>

                        {
                            lea_day === 1 ?

                                <div style={{ display: 'flex', flexDirection: 'row', width: 400, font: 20 }}>

                                    {/* {moment(date_start).format("DD/MM/YYYY")} */}
                                    {/* {JSON.stringify(moment(date_end).format("DD/MM/YYYY"))} */}

                                    <DatePicker
                                        style={{ marginTop: 20, width: 300 }}
                                        name="date_start"
                                        defaultValue={moment(date_start, dateFormat)} format={dateFormat}
                                    // onChange={(event) => this.onStartChange(event, "date_start")}                                  
                                    />
                                    <DatePicker
                                        style={{ marginTop: 20, width: 300 }}
                                        name="date_end"
                                        defaultValue={moment(date_end, dateFormat)} format={dateFormat}
                                    // onChange={(event) => this.onStartChange(event, "date_end")}

                                    />



                                </div>
                                : lea_day === 2 ?
                                    <div style={{ marginTop: 20 }}>
                                        <RadioGroup onChange={(event) => this.onChange_radio(event)} >
                                            {/* {JSON.stringify(moment(time_start).format("HH:mm:ss"))}
                                            {JSON.stringify(moment(time_end).format("HH:mm:ss"))}
                                            {JSON.stringify(moment(date_start).format("DD/MM/YYYY"))} */}

                                            <div style={{ display: 'flex', flexDirection: 'column' }}>
                                                <div style={{ display: 'flex', flexDirection: 'row' }}>
                                                    <Row >
                                                        <Col span={6}>
                                                            <Radio className="gutter-box" value="morning">Morning</Radio>
                                                        </Col>
                                                        {
                                                            radio === "morning" ?
                                                                <div style={{ textAlign: '-webkit-left' }}>
                                                                    <DatePicker
                                                                        style={{ marginTop: 20, width: 280, marginBottom: 20 }}


                                                                        name="date_end"
                                                                        format="DD-MM-YYYY "
                                                                        placeholder="From"
                                                                        onChange={(event) => this.onStartChange(event, "date_start")}
                                                                    // open={endOpen}
                                                                    // onOpenChange={this.handleEndOpenChange}
                                                                    />

                                                                    <Row >

                                                                        <Col span={6}>
                                                                            <TimePicker className="gutter-box" defaultValue={moment()} name="time_start"
                                                                                allowEmpty
                                                                                autoFocus={true}
                                                                                disabled={false}
                                                                                onChange={(event) => this._on_selecttime(event, "time_start")} />
                                                                        </Col>
                                                                        <Col span={6} style={{ marginLeft: 70 }}>
                                                                            <TimePicker className="gutter-box" defaultValue={moment()} name="time_end"
                                                                                allowEmpty
                                                                                autoFocus={true}
                                                                                disabled={false}
                                                                                onChange={(event) => this._on_selecttime(event, "time_end")} />

                                                                        </Col>
                                                                    </Row>
                                                                </div>
                                                                : null
                                                        }


                                                    </Row>
                                                </div>
                                                <div style={{ display: 'flex', flexDirection: 'row', marginTop: 20 }}>
                                                    <Row >
                                                        <Col span={6}>
                                                            <Radio className="gutter-box" value="afternoon">Afternoon</Radio>
                                                        </Col>

                                                        {
                                                            radio === "afternoon" ?
                                                                <div style={{ textAlign: '-webkit-left' }}>
                                                                    <DatePicker
                                                                        style={{ marginTop: 20, width: 280, marginBottom: 20 }}


                                                                        name="date_end"
                                                                        format="DD-MM-YYYY "
                                                                        placeholder="From"
                                                                        onChange={(event) => this.onStartChange(event, "date_start")}
                                                                    // open={endOpen}
                                                                    // onOpenChange={this.handleEndOpenChange}
                                                                    />
                                                                    <Row >

                                                                        <Col span={6}>
                                                                            <TimePicker className="gutter-box" defaultValue={moment()} name="time_start"
                                                                                allowEmpty
                                                                                autoFocus={true}
                                                                                disabled={false}
                                                                                onChange={(event) => this._on_selecttime(event, "time_start")} />
                                                                        </Col>
                                                                        <Col span={6} style={{ marginLeft: 70 }}>
                                                                            <TimePicker className="gutter-box" defaultValue={moment()} name="time_end"
                                                                                allowEmpty
                                                                                autoFocus={true}
                                                                                disabled={false}
                                                                                onChange={(event) => this._on_selecttime(event, "time_end")} />
                                                                        </Col>
                                                                    </Row>
                                                                </div>
                                                                : null
                                                        }
                                                    </Row>
                                                </div>
                                            </div>
                                        </RadioGroup>
                                    </div>
                                    : null
                        }







                        <div style={{ marginTop: 20 }}>
                            <TextArea rows={4} value={lea_reason} onChange={(event) => this.setState({ lea_reason: event.target.value })} />
                        </div>

                        <div style={{ width: '100%', marginBottom: 250, marginTop: 20 }}>
                            <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'center' }}>
                                <div style={{ width: 130, marginRight: 10 }} className="default_button" onClick={() => this._rejectrequest()}>
                                    Reject
                             </div>
                                <div style={{ width: 130 }} className="default_button" onClick={() => this._approverequest()}>
                                    Approve
                          </div>
                            </div>
                        </div>

                    </div>


                </div >
            </div >

        );
    }
}
export default testdatatable;
