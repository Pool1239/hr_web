import React, { Component } from 'react';
import { Input } from 'antd';
import { DatePicker, InputNumber, Upload, message, Radio, Row, Col, TimePicker, } from 'antd';
import "react-datepicker/dist/react-datepicker.css";
import moment from 'moment';
// Import React Table
import ReactTable from "react-table";
import "react-table/react-table.css";
import swal from 'sweetalert';
import { post } from '../service/service';


class LeaveRequest extends Component {
    constructor() {
        super();
        this.state = {
            data: [],
            moment: moment(),
            startValue: null,
            endValue: null,
            endOpen: false,
            value: 1,
            lea_day: "FullDay",
            leaName_type: null,
            date_start: moment().format("DD/MM/YYYY"),
            date_end: moment().format("DD/MM/YYYY"),
            time_start: moment().format("HH:mm:ss"),
            time_end: moment().format("HH:mm:ss"),
            lea_reason: null,
            radio: null,
        };
    }

    componentDidMount() {
        console.log("ข้อมูล", this.props.location, this.props.history)

        this.setState({
            ...this.props.location,
            leaName_type: this.props.location.state.leaName_type,
            leaID_type: this.props.location.state.leaID_type,
            empID: this.props.location.state.empID,
        })

        console.log("ข้อมูลที่มา", this.props.location.state)
    }


    on_summit = async () => {
        const {  lea_reason ,lea_day} = this.state
        let date_start
        let date_end
        if(lea_day){
           
            if(lea_day === "2" ){
                date_start= `${moment(this.state.date_start).format("YYYY-MM-DD")}  ${moment(this.state.time_start).format("HH:mm:ss")} `
                date_end = `${moment(this.state.date_start).format("YYYY-MM-DD")}  ${moment(this.state.time_end).format("HH:mm:ss")}`
            }else{
                
                date_start= moment(this.state.date_start).format("YYYY-MM-DD")
                date_end= moment(this.state.date_end).format("YYYY-MM-DD")
            }
            this.setState({date_start:date_start})
            this.setState({date_end:date_end})
            console.log("date_start",JSON.stringify(date_start))
            console.log("date_end",JSON.stringify(date_end))
        }
       
    
      

        const object = {
            empID: this.state.empID,
            lea_type: this.state.leaID_type,
            lea_day: this.state.lea_day,
            date_start:date_start,
            date_end:date_end,
            lea_reason: lea_reason

        }
        console.log("object", object)
        try {
            let res = await post('insert_leave', object)
            if (res.success == true) {
               
                swal({
                    title: "เข้าสู่ระบบสำเร็จ",

                    icon: "success",
                });
                setTimeout(()=>{
                    window.location.href = "/myleaverequest"
                },500)
                
            } else {
                alert(JSON.stringify(res.message))
            }
        } catch (error) {

        }

    }
    disabledEndDate = (endValue) => {
        const startValue = this.state.startValue;
        if (!endValue || !startValue) {
            return false;
        }
        return endValue.valueOf() <= startValue.valueOf();
    }


    onChange = (field, value) => {
        this.setState({
            [field]: value,
        });
    }

    onStartChange = (event, name) => {

        console.log("day", event)
        this.setState({ [name]: event })

        // this.setState({date_start:date_start})
        // console.log("date_start", moment(date_start).format("DD/MM/YYYY"))
    }

    onEndChange = (value) => {
        this.onChange('endValue', value);
    }

    handleStartOpenChange = (open) => {
        if (!open) {
            this.setState({ endOpen: true });
        }
    }

    handleEndOpenChange = (open) => {
        this.setState({ endOpen: open });
    }

    onChange_radio = (event) => {
        console.log('radio checked', event.target.value);
        this.setState({
            radio: event.target.value,
        });
    }
    _on_selecttime = (event, name) => {
        console.log("time", event)
        this.setState({ [name]: event })
    }
    render() {
        const RadioGroup = Radio.Group;
        const { TextArea } = Input;
        const { data } = this.state;
        const { startValue, date_start, date_end, lea_day, leaName_type, radio, time_end, time_start } = this.state;
        return (
            <div style={{ background: "#ffffff" }}>
                <div style={{ width: '100%', textAlign: '-webkit-center' }}>



                    <div style={{ fontSize: 20, color: '#00b8e6', borderBottomStyle: 'ridge', width: 180, marginTop: 30, textAlign: '-webkit-center' }}>
                        LeaveRequest
                    </div>

                    <div style={{ width: 400, marginTop: 40 }}>
                        <Input placeholder="Basic usage" value={leaName_type} />

                        <div style={{ width: 400, marginTop: 40 }} onChange={(event) => this.setState({ lea_day: event.target.value })}>
                            <div class="input-group mb-3">

                                <select class="custom-select" id="inputGroupSelect01">
                                    <option selected>Type</option>
                                    <option value="1">Full Day</option>
                                    <option value="2">Half Day</option>

                                </select>
                            </div>
                        </div>

                        {
                            lea_day === "1" ?

                                <div style={{ display: 'flex', flexDirection: 'row', width: 400, font: 20 }}>

                                    {/* {JSON.stringify(moment(date_start).format("DD/MM/YYYY"))}
                                    {JSON.stringify(moment(date_end).format("DD/MM/YYYY"))} */}
                                    <DatePicker
                                        style={{ marginTop: 20, width: 300 }}
                                        disabledDate={this.disabledStartDate}
                                        disabledTime={true}
                                        name="date_start"
                                        format="DD-MM-YYYY "
                                        placeholder="From"
                                        onChange={(event) => this.onStartChange(event, "date_start")}

                                    // onOpenChange={this.handleStartOpenChange}
                                    />
                                    <DatePicker
                                        style={{ marginTop: 20, width: 300 }}


                                        name="date_end"
                                        format="DD-MM-YYYY "
                                        placeholder="To"
                                        onChange={(event) => this.onStartChange(event, "date_end")}
                                    // open={endOpen}
                                    // onOpenChange={this.handleEndOpenChange}
                                    />



                                </div>
                                : lea_day === "2" ?
                                    <div style={{ marginTop: 20 }}>
                                        <RadioGroup onChange={(event) => this.onChange_radio(event)} >
                                            {/* {JSON.stringify(moment(time_start).format("HH:mm:ss"))}
                                            {JSON.stringify(moment(time_end).format("HH:mm:ss"))}
                                            {JSON.stringify(moment(date_start).format("DD/MM/YYYY"))} */}

                                            <div style={{ display: 'flex', flexDirection: 'column' }}>
                                                <div style={{ display: 'flex', flexDirection: 'row' }}>
                                                    <Row >
                                                        <Col span={6}>
                                                            <Radio className="gutter-box" value="morning">Morning</Radio>
                                                        </Col>
                                                        {
                                                            radio === "morning" ?
                                                                <div style={{ textAlign: '-webkit-left' }}>
                                                                    <DatePicker
                                                                        style={{ marginTop: 20, width: 280, marginBottom: 20 }}


                                                                        name="date_end"
                                                                        format="DD-MM-YYYY "
                                                                        placeholder="From"
                                                                        onChange={(event) => this.onStartChange(event, "date_start")}
                                                                    // open={endOpen}
                                                                    // onOpenChange={this.handleEndOpenChange}
                                                                    />

                                                                    <Row >

                                                                        <Col span={6}>
                                                                            <TimePicker className="gutter-box" defaultValue={moment()} name="time_start"
                                                                                allowEmpty
                                                                                autoFocus={true}
                                                                                disabled={false}
                                                                                onChange={(event) => this._on_selecttime(event, "time_start")} />
                                                                        </Col>
                                                                        <Col span={6} style={{ marginLeft: 70 }}>
                                                                            <TimePicker className="gutter-box" defaultValue={moment()} name="time_end"
                                                                                allowEmpty
                                                                                autoFocus={true}
                                                                                disabled={false}
                                                                                onChange={(event) => this._on_selecttime(event, "time_end")} />

                                                                        </Col>
                                                                    </Row>
                                                                </div>
                                                                : null
                                                        }


                                                    </Row>
                                                </div>
                                                <div style={{ display: 'flex', flexDirection: 'row', marginTop: 20 }}>
                                                    <Row >
                                                        <Col span={6}>
                                                            <Radio className="gutter-box" value="afternoon">Afternoon</Radio>
                                                        </Col>

                                                        {
                                                            radio === "afternoon" ?
                                                                <div style={{ textAlign: '-webkit-left' }}>
                                                                    <DatePicker
                                                                        style={{ marginTop: 20, width: 280, marginBottom: 20 }}


                                                                        name="date_end"
                                                                        format="DD-MM-YYYY "
                                                                        placeholder="From"
                                                                        onChange={(event) => this.onStartChange(event, "date_start")}
                                                                    // open={endOpen}
                                                                    // onOpenChange={this.handleEndOpenChange}
                                                                    />
                                                                    <Row >

                                                                        <Col span={6}>
                                                                            <TimePicker className="gutter-box" defaultValue={moment()} name="time_start"
                                                                                allowEmpty
                                                                                autoFocus={true}
                                                                                disabled={false}
                                                                                onChange={(event) => this._on_selecttime(event, "time_start")} />
                                                                        </Col>
                                                                        <Col span={6} style={{ marginLeft: 70 }}>
                                                                            <TimePicker className="gutter-box" defaultValue={moment()} name="time_end"
                                                                                allowEmpty
                                                                                autoFocus={true}
                                                                                disabled={false}
                                                                                onChange={(event) => this._on_selecttime(event, "time_end")} />
                                                                        </Col>
                                                                    </Row>
                                                                </div>
                                                                : null
                                                        }
                                                    </Row>
                                                </div>
                                            </div>
                                        </RadioGroup>
                                    </div>
                                    : null
                        }







                        <div style={{ marginTop: 20 }}>
                            <TextArea rows={4} onChange={(event) => this.setState({ lea_reason: event.target.value })} />
                        </div>

                        <div style={{ width: '100%', marginBottom: 300, marginTop: 20 }}>
                            <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'center' }}>
                                <div style={{ width: 130, marginRight: 10 }} className="default_button" onClick={() => window.location.href = "/"}>
                                    cancle
                             </div>
                                <div style={{ width: 130 }} className="default_button" onClick={() => this.on_summit()}>
                                    submit
                          </div>
                            </div>
                        </div>

                    </div>


                </div >
            </div >
        );
    }
}


export default LeaveRequest;