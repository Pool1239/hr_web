import React, { Component } from 'react';
import { Row, Input, Icon, Col } from 'react-materialize'
import { post } from '../service/service'



class LeaveRequest_Remaining extends Component {
    constructor() {
        super()
        this.state = {
            private: [],
            maxday: []
        }
    }


    Gotoflexrequst = () => {
        window.location.href = "/flexiblebenefitrequest"
    }

    Gotoleavereaust = () => {
        window.location.href = "/leaverequestuser"
    }
    componentWillMount = async () => {
        let res = localStorage.getItem('private')
        if (res) {
            let parse = JSON.parse(res)
            this.setState({
                private: parse
            }, () => console.log("ข้อมูลหน้าmy", this.state.private[0].empID))
        } else {
        }

        


    }

    componentDidMount = async () => {
        if (this.state.private[0].company) {
            try {
                let res = await post('/show_maxday', { id: this.state.private[0].company })
                this.setState({
                    maxday: res.result
                }, () => console.log(this.state.maxday)
                )
            } catch (error) {
                alert(error)
            }

        }

    }
    Gotoleavereaust = (element) => {
        this.setState({ element })
        setTimeout(() => {

        }, 100)

        console.log("ข้อมูลการลา", element)

        this.props.history.push({
            pathname: '/leaveRequestuser',
            search: `?the=${element.leaName_type}`,
            state: {
                leaName_type: element.leaName_type,
                leaID_type: element.leaID_type,
                empID: this.state.private[0].empID
            }
        })

    }

    render() {

        return (
            <div>
                <div style={{ width: '100%' }}>



                    <div style={{ fontSize: 30, textAlign: 'center', marginTop: 40 }}>
                        LeaveRequest&Remaining

                    </div>
                    <div style={{ width: '100%', display: 'flex', justifyContent: 'center' }}>
                        <div style={{ fontSize: 30, textAlign: 'center' }}>

                            <p style={{ borderBottom: '2px solid #1a75ff', width: 150, color: '#1a75ff', fontWeight: 'bold' }}>Public Holiday</p>
                        </div>
                    </div>
                </div>

                <div style={{ textAlign: '-webkit-center' }}>

                    {
                        this.state.private.map((e) => {
                            return (
                                <div>
                                    <div style={{ fontSize: 30, fontWeight: 'bold' }}>
                                        <p style={{ borderBottomStyle: 'ridge', width: 300 }}>{e.full_name}</p>
                                    </div>
                                    <div >

                                        {
                                            this.state.maxday.map((element) => {
                                                return (
                                                    <div>

                                                        <div onClick={() => this.Gotoleavereaust(element)} style={{ width: '70%', display: 'flex', justifyContent: 'center' }}>
                                                            <div className="borders"  >
                                                                <div style={{ width: '50%' }} >
                                                                    <p>{element.leaName_type}  {element.leaName_thai}</p>

                                                                </div>
                                                                <p className="borders_p">{`${element.leaID_type} / ${element.max_day}`}</p>
                                                                <p className="borders_p_2">day</p>

                                                            </div>
                                                        </div>

                                                    </div>
                                                )
                                            })

                                        }

                                        <Col s={4}>

                                            <div style={{ width: '70%', display: 'flex', justifyContent: 'center' }}>
                                                <div onClick={() => this.Gotoflexrequst()} className="borders" style={{ cursor: 'pointer', display: 'flex', flexDirection: 'column', alignItems: 'center' }} >

                                                    <p>Flexible Benefit Request</p>
                                                    <p className="borders_p_2 ">คำขอเกี่ยวกับสวัสดิการอื่นๆ</p>
                                                </div>

                                            </div>
                                        </Col>



                                    </div>
                                </div>
                            )


                        })
                    }

                </div>


                {/* <div >
                    <Row>

                        <Col s={6}>
                            <div   onClick={()=>this.Gotoleavereaust()} style={{ width: '100%', display: 'flex', justifyContent: 'center' }}>
                                <div className="borders"  >
                                    <div style={{ width: '50%' }} >
                                        <p>Annual Leave</p>
                                        <p className="borders_p_2 ">ลาพักหยุดประจำปี</p>

                                    </div>
                                    <p className="borders_p">5/10</p>
                                    <p className="borders_p_2">day</p>

                                </div>
                            </div>
                        </Col>

                        <Col s={6}>
                            <div onClick={()=>this.Gotoleavereaust()} style={{ width: '100%', display: 'flex', justifyContent: 'center' }}>
                                <div className="borders" >
                                    <div style={{ width: '50%' }} >
                                        <p>Enter for Leave</p>
                                        <p className="borders_p_2 ">ลาบวช</p>
                                    </div>
                                    <p className="borders_p">5/10</p>
                                    <p className="borders_p_2">day</p>
                                </div>

                            </div>
                        </Col>
                    </Row>
                    <Row>
                        <Col s={6}>
                            <div onClick={()=>this.Gotoleavereaust()} style={{ width: '100%', display: 'flex', justifyContent: 'center' }}>
                                <div className="borders" >
                                    <div style={{ width: '50%' }} >
                                        <p>Personal Leave</p>
                                        <p className="borders_p_2 ">ลากิจ</p>
                                    </div>
                                    <p className="borders_p">5/10</p>
                                    <p className="borders_p_2">day</p>
                                </div>

                            </div>
                        </Col>
                        <Col s={6}>
                            <div onClick={()=>this.Gotoleavereaust()} style={{ width: '100%', display: 'flex', justifyContent: 'center' }}>
                                <div className="borders" >
                                    <div style={{ width: '50%' }} >
                                        <p>Military Leave</p>
                                        <p className="borders_p_2 ">ลาเพื่อรับราชการทหาร</p>
                                    </div>
                                    <p className="borders_p">5/10</p>
                                    <p className="borders_p_2">day</p>
                                </div>

                            </div>
                        </Col>
                    </Row>
                    <Row>
                        <Col s={6}>
                            <div  onClick={()=>this.Gotoleavereaust()}style={{ width: '100%', display: 'flex', justifyContent: 'center' }}>
                                <div className="borders" >
                                    <div style={{ width: '50%' }} >
                                        <p>Sick Leave</p>
                                        <p className="borders_p_2 ">ลาป่วย</p>
                                    </div>
                                    <p className="borders_p">5/10</p>
                                    <p className="borders_p_2">day</p>
                                </div>

                            </div>
                        </Col>
                        <Col s={6}>
                            <div onClick={()=>this.Gotoleavereaust()} style={{ width: '100%', display: 'flex', justifyContent: 'center' }}>
                                <div className="borders" >
                                    <div style={{ width: '50%' }} >
                                        <p>maternity Leave</p>
                                        <p className="borders_p_2 ">ลาคลอด</p>
                                    </div>
                                    <p className="borders_p">5/10</p>
                                    <p className="borders_p_2">day</p>
                                </div>

                            </div>
                        </Col>

                    </Row>
                    <Row>
                        <Col s={6}>
                            <div  onClick={()=>this.Gotoleavereaust()} style={{ width: '100%', display: 'flex', justifyContent: 'center' }}>
                                <div className="borders" >
                                    <div style={{ width: '50%' }} >
                                        <p>Training Leave</p>
                                        <p className="borders_p_2 ">ลาเพื่อฝึกอบรม</p>
                                    </div>
                                    <p className="borders_p">5/10</p>
                                    <p className="borders_p_2">day</p>
                                </div>

                            </div>

                        </Col>
                        <Col s={6}>

                            <div  style={{ width: '100%', display: 'flex', justifyContent: 'center' }}>
                                <div  onClick={()=>this.Gotoflexrequst()} className="borders" style={{ width: '60%', cursor: 'pointer',display:'flex',flexDirection:'column',alignItems:'center' }} >
                                
                                    <p>Flexible Benefit Request</p>
                                    <p className="borders_p_2 ">คำขอเกี่ยวกับสวัสดิการอื่นๆ</p>
                                </div>

                            </div>
                        </Col>

                    </Row>
                </div>
 */}



            </div >
        );
    }
}

export default LeaveRequest_Remaining;
