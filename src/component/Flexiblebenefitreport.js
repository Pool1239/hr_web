import React, { Component } from 'react';
import { DatePicker, InputNumber, Input, message, AutoComplete, Row, Col, TimePicker, } from 'antd';
import "react-datepicker/dist/react-datepicker.css";
import moment from 'moment';
import "antd/dist/antd.css";
import { ip } from '../service/service';


class Flexiblebenefitreport extends Component {
    constructor() {
        super()
        this.state = {
            dataSource: [],
            date1: moment().format("YYYY-MM-DD"),
            date2: moment().format("YYYY-MM-DD"),
        }
    }

    componentWillMount() {
        let ress = localStorage.getItem('private')
        if (ress) {
            console.log("res", JSON.parse(ress))
            let parse = JSON.parse(ress)
            this.setState({
                empID: parse[0].empID,
                role: parse[0].role,
                private: parse[0],

            }, () => console.log("ข้อมูลส่วนตัวreport", this.state.private))
        }



    }
    _on_exportdata = async () => {



        const company = this.state.private.company
        console.log("company", company)
        const { date1, date2 } = this.state
        var win = window.open(ip + "export_benefit_by_company/" + moment(date1).format("YYYY-MM-DD") + "/" + moment(date2).format("YYYY-MM-DD") + "/" + company + '_blank');
        win.focus();
        setTimeout(() => {
            window.location.reload();
        }, 500)


    }
    onStartChange = (event, name) => {

        console.log("day", event)
        this.setState({ [name]: event })

        // this.setState({date_start:date_start})
        // console.log("date_start", moment(date_start).format("DD/MM/YYYY"))
    }




    render() {
        const { MonthPicker, RangePicker } = DatePicker;
        const { startValue, endValue, date2, date1 } = this.state;
        const InputGroup = Input.Group;
        const dateFormat = 'DD-MM-YYYY'
        return (
            <div>
                <div style={{ width: '100%', textAlign: '-webkit-center' }}>
                    <div style={{ fontSize: 30, fontWeight: 'bold' }}>
                        <p style={{ borderBottomStyle: 'ridge', width: 300 }}>Flexi Blebenefit Report</p>
                    </div>



                    <div style={{ display: 'flex', flexDirection: 'row', width: 400 }}>
                        {/* {JSON.stringify(moment(date1).format("DD-MM-YYYY"))}
                        {JSON.stringify(moment(date2).format("DD-MM-YYYY"))} */}
                        <DatePicker
                            style={{ marginTop: 20, width: 300 }}
                            disabledDate={this.disabledStartDate}
                            disabledTime={true}
                            name="date1"
                            format="DD-MM-YYYY "
                            placeholder="From"
                            onChange={(event) => this.onStartChange(event, "date1")}


                        />
                        <DatePicker
                            style={{ marginTop: 20, width: 300 }}
                            disabledDate={this.disabledStartDate}
                            disabledTime={true}
                            name="date2"
                            format="DD-MM-YYYY "
                            placeholder="To"
                            onChange={(event) => this.onStartChange(event, "date2")}

                        />


                    </div>
                    <div style={{ width: 400 }}>
                        {/* <Input style={{ marginTop: 20 }} placeholder="Export to Email :" /> */}





                        <div style={{ width: '100%', marginBottom: 250, marginTop: 20 }}>
                            <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between' }}>
                                <div style={{ width: 130, marginRight: 10 }} className="default_button" onClick={() => window.location.href = "/"}>
                                    cancle
                             </div>
                                <div style={{ width: 130, justifyContent: 'space-between' }} className="default_button" onClick={() => this._on_exportdata()}>
                                    Export
                          </div>
                            </div>
                        </div>
                    </div>






                </div>
            </div>

        );
    }
}

export default Flexiblebenefitreport;