import React, { Component } from 'react';
import { DatePicker, InputNumber, Input, message, AutoComplete, Row, Col, TimePicker, } from 'antd';
import "react-datepicker/dist/react-datepicker.css";
import moment from 'moment';
import "antd/dist/antd.css";
import {post} from '../service/service'



class Createuseraccount extends Component {
    constructor() {
        super()
        this.state = {
            dataSource: [],
            date:null,
            username:null,
            password:null,
            emp_name:null,
            emp_last:null,
            role:null,
            email:null,
            pro_name:null,
            empID:""
            

        }
    }

    componentDidMount = async () => {
        let res = localStorage.getItem('data')
        if(res){
        let parse = JSON.parse(res)
        this.setState({
          empID: parse[0].empID
        })
        } else{
        }
    
       
      }
    _onCreate_account = async () => {



        let object = {
            user_create_id:this.state.empID,
            username:this.state.username,
            password:this.state.password,
            emp_name:this.state.emp_name,
            emp_last:this.state.emp_last,
            role:this.state.role,
            email:this.state.email,
            pro_name:this.state.pro_name,
           
        };
       
        console.log("object",object)
    
        try {
          let res = await post("register",object)
          if(res.success == true){
            alert(JSON.stringify(res.message))
          }else{
            alert(JSON.stringify(res.message))
          }
          
        } catch (error) {
          alert(error);
        }
      };

    _on_input = (event)=>{
        console.log("ข้อมูล",event.target.value)
        this.setState({[event.target.name]:event.target.value})
    }


    render() {
        const { MonthPicker, RangePicker } = DatePicker;
        const InputGroup = Input.Group;
        const dateFormat = 'DD-MM-YYYY'
        const {dataSource: [],
            date,username, password, emp_name, emp_last, role, email,pro_name} =this.state
        return (
            <div>
                <div style={{ width: '100%', textAlign: '-webkit-center' }}>
                    <div style={{ fontSize: 30, fontWeight: 'bold' }}>
                        <p style={{ borderBottomStyle: 'ridge', width: 300 }}> Create User Account</p>
                    </div>
                    <div style={{ width: 400 }} onChange={(event)=>this._on_input(event)}>
                        {/* <Input style={{ marginTop: 20 }} placeholder="Employee No." /> */}
                        <Input style={{ marginTop: 20 }} placeholder="Name" name="emp_name" value={emp_name}/>
                        <Input style={{ marginTop: 20 }} placeholder="Surname"name="emp_last" value={emp_last}/>
                        <Input style={{ marginTop: 20 }} placeholder="Email" name="email"value={email}/>
                        {/* <DatePicker style={{ marginTop: 20, width: 400 }} value={date} name="date"  defaultValue={moment()} format={dateFormat} onChange={(event)=>console.log("วันที่่",event)} /> */}
                        <div style={{ width: 400, marginTop: 20 }} name="pro_name"value={pro_name} onChange={(event) => this.setState({ pro_name: event.target.value })}>
                            <div class="input-group mb-3">

                                <select class="custom-select" id="inputGroupSelect01" >
                                  
                                    <option value="1">Pass</option>
                                    <option value="2">4 Month</option>

                                </select>
                            </div>
                        </div>
                        <Input style={{ marginTop: 20 }} placeholder="Username" name="username"value={username}/>
                        <Input style={{ marginTop: 20 }} placeholder="Password" name="password"value={password}/>
                        <div style={{ width: 400, marginTop: 20 }}name="role" onChange={(event) => this.setState({ role: event.target.value })}>
                            <div class="input-group mb-3">

                                <select class="custom-select" id="inputGroupSelect01" >
                                    <option selected>User Role</option>
                                    <option value="2">Admin</option>
                                    <option value="1">Employee</option>

                                </select>
                            </div>
                        </div>


                        <div style={{ width: '100%', marginBottom: 200, marginTop: 20 }}>
                            <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between' }}>
                                <div style={{ width: 130, marginRight: 10 }} className="default_button" onClick={() => window.location.href = "/"}>
                                    cancle
                             </div>
                                <div style={{ width: 130 ,   justifyContent: 'space-between' }} className="default_button" onClick={()=>this._onCreate_account()}>
                                    Create Account
                          </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        );
    }
}

export default Createuseraccount;