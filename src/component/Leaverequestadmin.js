import React, { Component } from 'react';
import { Input } from 'antd';
import { DatePicker, InputNumber, Upload, message, Radio, Row, Col, TimePicker, } from 'antd';
import "react-datepicker/dist/react-datepicker.css";
import moment from 'moment';
import { Link, Redirect } from 'react-router-dom'
// Import React Table
import ReactTable from "react-table";
import "react-table/react-table.css";
import { get, post } from '../service/service'
import { Icon, Modal } from 'antd';
import { Button } from 'reactstrap';
import swal from 'sweetalert';
import Testdatatable from '../component/Testdatatable';
import { element } from 'prop-types';


class Leaverequestadmin extends Component {
    state = { visible: false }
    constructor() {
        super();
        this.state = {
            data: [],
            moment: moment(),
            startValue: null,
            endValue: null,
            endOpen: false,
            value: 1,
            duration: "FullDay",
            datatable: [],
            leaID: null,
            leave_request: [],
            name: null,
            visit: null,
            action: null,
            Empid:null,
            leaveID:null,
        };

    }
    
    
    showModal = (element) => {
        console.log("ele",element)
        this.setState({element})
        console.log("eleafter",element)
    
        this.setState({
            visible: true,
        });
    }

    handleOk = (e) => {
        console.log(e);
        this.setState({
            visible: false,
        });
    }

    handleCancel = (e) => {
        console.log(e);
        this.setState({
            visible: false,
        });
    }
   
    disabledStartDate = (startValue) => {
        const endValue = this.state.endValue;
        if (!startValue || !endValue) {
            return false;
        }
        return startValue.valueOf() > endValue.valueOf();
    }

    disabledEndDate = (endValue) => {
        const startValue = this.state.startValue;
        if (!endValue || !startValue) {
            return false;
        }
        return endValue.valueOf() <= startValue.valueOf();
    }

    onChange = (field, value) => {
        this.setState({
            [field]: value,
        });
    }

    onStartChange = (value) => {
        this.onChange('startValue', value);
    }

    onEndChange = (value) => {
        this.onChange('endValue', value);
    }

    handleStartOpenChange = (open) => {
        if (!open) {
            this.setState({ endOpen: true });
        }
    }

    handleEndOpenChange = (open) => {
        this.setState({ endOpen: open });
    }

    onChange_radio = (event) => {
        console.log('radio checked', event.target.value);
        // this.setState({
        //     value: e.target.value,
        // });
    }
    // _approverequest = async (element) => {
    //     let res = localStorage.getItem('private')
    //     let add_empID = JSON.parse(res)[0].empID

    //     let  leaveID  = this.state.leaveID;              

    //     swal({
    //         title: "Are You Sure To Approve?",
    //         icon: "warning",
    //         buttons: true,
    //         dangerMode: true,
    //     })
    //         .then((willapprove) => {
    //             if (willapprove) {
    //                 this._approveyet(add_empID,leaveID)

    //             } else {
    //                 swal("Fail");
    //             }
    //         });
    // }
    // _approveyet = async (add_empID,leaveID) => {
       

    //     try {

    //         let res = await post('/update_leave_request',{ add_empID, leaveID } )
    //         if (res.success) {
    //             swal("Complete", {
    //                 icon: "success",
    //             });
    //             window.location.href = "/leaverequestadmin"
    //         } else {
    //             console.log("ผิดพลาด")
    //         }


    //     } catch (err) {
    //         console.log(err)
    //     }
    // }
    

    componentWillMount = async () => {

        try {
            let res = await get('/leave_request')
            this.setState({
                datatable: res.result
            }, () => console.log("ข้อมูลตารางหน้าleaverequest", this.state.datatable)
            )
        } catch (error) {
            alert(error)
        }

        setTimeout(()=>{
            this._on_pushdatatable(this.state.datatable)
        },100)
        this.setState({
            ...this.props
        })
        console.log("ข้อมูลที่มา", this.props)

    }
    _on_pushdatatable=(datatable)=>{
        let boxdata=[]
        console.log("datatable",datatable)
        this.state.datatable.map((element)=>{
            boxdata.push({
                ...element,
                visit: <div style={{width:50}} onClick={()=>this.showModal(element)} className="default_button">ดูข้อมูล</div>,
              action:<div style={{width:70}} onClick={()=>this.update_approve(element)} className="default_button">Approve</div>
            })
        })
        this.setState({boxdata:boxdata})
        console.log("boxdata",boxdata)
    }
    update_approve= async(element)=>{
        let res = localStorage.getItem('private')
        let add_empID = JSON.parse(res)[0].empID
        
        let  leaveID  = element.leaveID;
        try {
            let res = await post('/update_leave_request',{ add_empID, leaveID } )
            window.location.href = "/leaverequestadmin"
        } catch (error) {
            
        }
    }

    render() {
        
        const { startValue, endValue, endOpen, datatable,boxdata} = this.state;

        return (
            <div style={{ background: "#ffffff", fontSize: 20 }}>
                <div style={{ width: '100%', textAlign: '-webkit-center' }}>



                    <div style={{ fontSize: 50, width: 180, marginTop: 30, fontWeight: 'bold' }}>
                        LeaveRequest
                    </div>


                    <div style={{ width: 800, display: 'flex', justifyContent: 'center', borderTopStyle: 'ridge' }}>

                         
                        <div style={{ width: 800, backgroundColor: '#ffffff', marginTop: 20 }}>
                            
                               
                                        <ReactTable
                                            data={boxdata}
                                            columns={[
                                                {
                                                    // Header: "Name",
                                                    columns: [
                                                        {
                                                            Header: "ID",
                                                            accessor: "leaveID"
                                                        },
                                                        {
                                                            Header: "EmployeeName",
                                                            accessor: "full_name",

                                                        }
                                                    ]
                                                },
                                                {
                                                    // Header: "Info",
                                                    columns: [
                                                        {
                                                            Header: "LeaveType",
                                                            accessor: "leaName_type"
                                                        },
                                                        {
                                                            Header: "Type",
                                                            accessor: "leaName_day"
                                                        }
                                                    ]
                                                },
                                                {
                                                    // Header: 'Stats',
                                                    columns: [
                                                        {
                                                            Header: "From",
                                                            accessor: "date_start"
                                                        },
                                                        {
                                                            Header: "To",
                                                            accessor: "date_end"
                                                        },
                                                        {
                                                            Header: "No.of day",
                                                            accessor: "lea_day"
                                                        },
                                                        {
                                                            Header: "Status",
                                                            accessor: "sta_name"
                                                        },
                                                        {
                                                            Header: "Approver",
                                                            accessor: "nameuser"
                                                        },
                                                        {
                                                            Header: "Created",
                                                            accessor: "date_created"
                                                        },

                                                        {
                                                            Header: "Detail",
                                                            accessor: "visit",
                                                            // Cell: () => this._getdetail(element)
                                                        },
                                                        {
                                                            Header: "Action",
                                                            accessor: "action",
                                                            // Cell: props => this._getapprove(props)
                                                        },



                                                    ]
                                                }
                                            ]}
                                            defaultPageSize={10}

                                            className="-striped -highlight"
                                        />
                                    
                                

                            <br />
                        </div>
                    </div>
                    <Modal
                        
                        visible={this.state.visible}
                        onOk={this.handleOk}
                        onCancel={this.handleCancel}
                    >
                        <Testdatatable  {...this.state.element}/>
                    </Modal>
                </div >
            </div >
        );
    }
}


export default Leaverequestadmin;