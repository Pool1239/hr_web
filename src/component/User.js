import React, { Component } from 'react';
import { Row, Input, Icon, Col } from 'react-materialize'
import { get, post } from '../service/service';
import swal from 'sweetalert';
import ReactTable from "react-table";
import "react-table/react-table.css";
import { Button } from 'reactstrap';

class User extends Component {
    constructor() {
        super();
        this.state = {
            data: [],
            visit: null,
            empID: null,
            employee: [],
        };
    }
    componentDidMount = async () => {
        try {
            let res = await get('/show_user')
            this.setState({
                data: res.result
            }, () => console.log(this.state.data)
            )
        } catch (error) {
            alert(error)
        }
    }
    _deleterequest = (props) => {
        let {empID} = props.original;
        console.log(props)        
        return <div style={{ marginLeft: 10, cursor: 'pointer' }} onClick={() => this._on_delete_value(empID)}>
            <Button color="danger" size="sm">Delete</Button>
        </div>
    }

   
    _on_delete_value = (empID) => {
        let seleck = []
        let emp_id_data

        swal({
            title: "คุณต้องการลบข้อมูลใช่หรือไม่?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    this.deletevalue(empID)

                } else {
                    swal("ลบข้อมูลไม่สำเร็จ");
                }
            });
    }
    deletevalue = async (empID) => {
        let seleck = []
        let emp_id_data
        // if (typeof empID === "object") {

        //     emp_id_data = JSON.stringify(empID)
        // } else {
        //     emp_id_data = JSON.stringify([empID])
        // }

        // const object = {
        //     empID: emp_id_data,
        // }

        // alert(JSON.stringify(object))
        try {

               let res = await post( '/del_user',{empID}) 
                    if (res.success) {
                        swal("ลบข้อมูลสำเร็จ", {
                            icon: "success",
                        });
                        window.location.href = "/user"
                    } else {
                        console.log("ผิดพลาด")
                    }


        } catch (err) {
            console.log(err)
        }
    }

    render() {
        const { data } = this.state;
        return (
            <div style={{ width: '100%', fontSize: 20 }}>
                <div style={{ width: '100%', fontSize: 30, borderBottomStyle: 'ridge' }}>

                    <div style={{ width: 200, position: 'absolute', fontSize: 50, display: ' flex', justifyContent: 'space-around', fontWeight: 'bold' }} > User</div>
                    <div style={{ fontSize: 20, color: '#00b8e6', marginLeft: 30, padding: 10, cursor: 'pointer', textAlign: 'right' }} onClick={() => window.location.href = "/createuseraccount"}>
                        +Create User Account
                    </div>

                </div>
                <div style={{ width: '100%', textAlign: '-webkit-center' }}>




                    <div style={{ width: '100%', display: 'flex', justifyContent: 'center' }}>


                        <div style={{ width: 1000, backgroundColor: '#ffffff', marginTop: 40 }}>
                            <ReactTable
                                data={data}
                                columns={[
                                    {
                                        // Header: "Name",
                                        columns: [
                                            {
                                                Header: "ID",
                                                accessor: "empID"
                                            },
                                            {
                                                Header: "EmployeeName",
                                                accessor: "full_name",
                                            }
                                        ]
                                    },
                                    {
                                        // Header: "Info",
                                        columns: [
                                            {
                                                Header: "Email",
                                                accessor: "email"
                                            },
                                            {
                                                Header: "Starting Date",
                                                accessor: "starting_date"
                                            }
                                        ]
                                    },
                                    {
                                        // Header: 'Stats',
                                        columns: [
                                            {
                                                Header: "Probation period",
                                                accessor: "pro_name"
                                            },
                                            {
                                                Header: "Approval Email",
                                                accessor: "admin_email"
                                            },
                                            {
                                                Header: "Role",
                                                accessor: "rol_name"
                                            },
                                        ]
                                    },
                                    {

                                        columns: [
                                            {
                                                Header: "Visit",
                                                accessor: "visit",
                                                Cell: props => this._deleterequest(props)
                                            },

                                        ]
                                    }
                                ]}
                                defaultPageSize={10}

                                className="-striped -highlight"
                            />
                            <br />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}


export default User;