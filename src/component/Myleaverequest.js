import React, { Component } from 'react';
import { get, post } from '../service/service'
import "antd/dist/antd.css";
import { Row, Input, Icon, Col } from 'react-materialize'




class Myleaverequest extends Component {
  constructor() {
    super()
    this.state = {
      private: [],
      maxday: []
    }
  }
  
  componentWillMount = async () => {
    let res = localStorage.getItem('private')
    if (res) {
      let parse = JSON.parse(res)
      this.setState({
        private: parse
      }, () => console.log("ข้อมูลหน้าmy",this.state.private[0].empID))
    } else {
    }


  }

  componentDidMount = async () => {
    if (this.state.private[0].company) {
      try {
        let res = await post('/show_maxday', { id: this.state.private[0].company })
        this.setState({
          maxday: res.result
        }, () => console.log(this.state.maxday)
        )
      } catch (error) {
        alert(error)
      }

    }

  }

  Gotoflexrequst = () => {
    window.location.href = "/flexiblebenefitrequest"
  }

  Gotoleavereaust = (element) => {
    this.setState({element})
    setTimeout(() => {

    }, 100)
  
    console.log("ข้อมูลการลา",element)
  
    this.props.history.push({
      pathname: '/leaveRequestuser',
      search: `?the=${element.leaName_type}`, 
      state: {leaName_type:element.leaName_type ,
        leaID_type:element.leaID_type,
        empID:this.state.private[0].empID
      }
    })
   
  }


  render() {


    return (
      <div>
        <div style={{ width: '100%', textAlign: '-webkit-center' }}>


          <div>

            {
              this.state.private.map((e) => {
                return (
                  <div>
                    <div style={{ fontSize: 30, fontWeight: 'bold' }}>
                      <p style={{ borderBottomStyle: 'ridge', width: 300 }}>{e.full_name}</p>
                    </div>
                    <div >

                      {
                        this.state.maxday.map((element) => {
                          return (
                            <div>

                              <div onClick={() => this.Gotoleavereaust(element)} style={{ width: 600, display: 'flex', justifyContent: 'center' }}>
                                <div className="borders"  >
                                  <div style={{ width: '50%' }} >
                                    <p>{element.leaName_type}  {element.leaName_thai}</p>
                                    
                                  </div>
                                  <p className="borders_p">{`${element.leaID_type} / ${element.max_day}`}</p>
                                  <p className="borders_p_2">day</p>

                                </div>
                              </div>

                            </div>
                          )
                        })

                      }

                      <Col s={4}>

                        <div style={{ width: 600, display: 'flex', justifyContent: 'center' }}>
                          <div onClick={() => this.Gotoflexrequst()} className="borders" style={{ cursor: 'pointer', display: 'flex', flexDirection: 'column', alignItems: 'center' }} >

                            <p>Flexible Benefit Request</p>
                            <p className="borders_p_2 ">คำขอเกี่ยวกับสวัสดิการอื่นๆ</p>
                          </div>

                        </div>
                      </Col>



                    </div>
                  </div>
                )


              })
            }

          </div>







        </div>
      </div>
    );
  }
}

export default Myleaverequest;