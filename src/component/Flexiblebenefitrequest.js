import React, { Component } from 'react';
import moment from 'moment';

import { DatePicker, InputNumber, Upload, message} from 'antd';
import "react-datepicker/dist/react-datepicker.css";



class Flexiblebenefitrequest extends Component {
    constructor() {
        super();
        this.state = {
            moment: moment(),
            startValue: null,
            endValue: null,
            endOpen: false,
           date1: moment().format("DD/MM/YYYY"),
           date2:moment().format("DD/MM/YYYY"),

        }
    }



    disabledStartDate = (startValue) => {
        const endValue = this.state.endValue;
        if (!startValue || !endValue) {
            return false;
        }
        return startValue.valueOf() > endValue.valueOf();
    }

    disabledEndDate = (endValue) => {
        const startValue = this.state.startValue;
        if (!endValue || !startValue) {
            return false;
        }
        return endValue.valueOf() <= startValue.valueOf();
    }

    onChange = (field, value) => {
        this.setState({
            [field]: value,
        });
    }

    onStartChange = (value) => {
        this.onChange('startValue', value);
    }

    onEndChange = (value) => {
        this.onChange('endValue', value);
    }

    handleStartOpenChange = (open) => {
        if (!open) {
            this.setState({ endOpen: true });
        }
    }

    handleEndOpenChange = (open) => {
        this.setState({ endOpen: open });
    }
    onChange(info) {
        if (info.file.status !== 'uploading') {
            console.log(info.file, info.fileList);
        }
        if (info.file.status === 'done') {
            message.success(`${info.file.name} file uploaded successfully`);
        } else if (info.file.status === 'error') {
            message.error(`${info.file.name} file upload failed.`);
        }
    }
    onStartChange = (event, name) => {

        console.log("day", event)
        this.setState({ [name]: event })

       
    }



    render() {


        const {  date1,date2 } = this.state;

        return (
            <div>
                <div style={{ width: '100%', textAlign: '-webkit-center' }}>

                    <div style={{ fontSize: 20, color: '#00b8e6', borderBottomStyle: 'ridge', width: 180, marginTop: 30 }}>
                        Flexiblebenefitrequest
                </div>
                    <div style={{ width: 300, marginTop: 40 }}>
                        <div class="input-group mb-3">

                            <select class="custom-select" id="inputGroupSelect01">
                                <option selected>Choose...</option>
                                <option value="1">Perpose for Flexible Benefit</option>
                                <option value="2">Optical Purchasing</option>
                                <option value="3">Oral Purchasing</option>
                                <option value="3">Sport</option>
                                <option value="3">Transportation on vacation</option>
                            </select>
                        </div>
                    </div>

                    <div style={{ display: 'flex', flexDirection: 'column', width: 300 }}>
                    {JSON.stringify(moment(date1).format("DD/MM/YYYY"))}
                    {JSON.stringify(moment(date2).format("DD/MM/YYYY"))}
                    <DatePicker
                            style={{ marginTop: 20, width: 300 }}
                            disabledDate={this.disabledStartDate}
                            disabledTime={true}
                            name="date1"
                            format="DD-MM-YYYY "
                            placeholder="Date Request"
                            onChange={(event) => this.onStartChange(event, "date1")}


                        />
                        <DatePicker
                            style={{ marginTop: 20, width: 300 }}


                            name="date2"
                            format="DD-MM-YYYY "
                            placeholder="Date on Receipt"
                            onChange={(event) => this.onStartChange(event, "date2")}

                        />

                        <InputNumber style={{ marginTop: 20, width: 300 }} min={1} max={1000} placeholder="0.0" onChange={() => this.onChange()} />

                        <div style={{ marginTop: 20, width: 300 }}>
                            <Upload  style={{color: '#00b8e6',borderBottomStyle: 'ridge',cursor:'pointer'}}>
                               
                                     Upload Receipt
                            
                            </Upload>
                        </div>


                    </div>
                    <div style={{ width: '100%', marginBottom: 300, marginTop: 20 }}>
                        <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'center' }}>
                            <div style={{ width: 130, marginRight: 10 }} className="default_button" onClick={()=>window.location.href="/"}>
                                cancle
                             </div>
                            <div style={{ width: 130 }} className="default_button" >
                                submit
                          </div>


                        </div>
                    </div>
                </div>


            </div>
        );
    }
}


export default Flexiblebenefitrequest;