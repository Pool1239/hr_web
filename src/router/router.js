import React, { Component } from 'react'
import { Router, Route } from 'react-router-dom'
import App from '../App'
import LeaveRequest_Remaining from '../component/LeaveRequest_Remaining'
import HomeActivity from '../component/Homeactivity'
import LeaveRequestuser from '../component/LeaveRequestuser'
import login from '../screen/login/login'
import register from '../screen/login/register'
import User from '../component/User'
import Myleaverequest from '../component/Myleaverequest'
import Flexiblebenefitrequest from '../component/Flexiblebenefitrequest'
import Createuseraccount from '../component/Createuseraccount'
import LeaveReport from '../component/Leavereport'
import Flexiblebenefitreport from '../component/Flexiblebenefitreport'
import Leaverequestadmin from '../component/Leaverequestadmin'
import Testdatatable from '../component/Testdatatable'

class RouterChild extends Component {
    render() {
        return (
            <div>
                <Route exact path="/" component={HomeActivity} />
                <Route path="/leaverequest_remaining" component={LeaveRequest_Remaining} />
                <Route path="/leaveRequestuser" component={LeaveRequestuser} />
                <Route path="/user" component={User} />
                <Route path="/myleaverequest" component={Myleaverequest} />
                <Route path="/flexiblebenefitrequest" component={Flexiblebenefitrequest} />
                <Route path="/login" component={login} />
                <Route path="/register" component={register} />
                <Route path="/createuseraccount" component={Createuseraccount} />
                <Route path="/leavereport" component={LeaveReport} />
                <Route path="/flexiblebenefitreport" component={Flexiblebenefitreport} />
                <Route path="/leaverequestadmin" component={Leaverequestadmin} />
                <Route path="/testdatatable" component={Testdatatable} />
                
            </div>
        )
    }
}
export default RouterChild