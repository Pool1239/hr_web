import React, { Component, useState } from 'react'
import Banner from '../../asset/logo/logo.png'
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import { Input, Icon } from 'antd';
import pjs from './pjss.png';
import TextField from '@material-ui/core/TextField';
import { post } from '../../service/service'
import swal from 'sweetalert'


const styles = {
  margin: 10,
  width: 60,
  height: 60
  // margin: theme.spacing.unit,
  // backgroundColor: theme.palette.secondary.main,
};
const divStyle = {
  textAlign: "-webkit-center"
};

class register extends Component {
  constructor() {
    super();
    this.state = {
      email: null,
      password: null,
      confirm: null,
      empID: ""
    };
  }
  oninput = event => {
    this.setState({ [event.target.name]: event.target.value });
  };
  componentDidMount = async () => {
    let res = localStorage.getItem('data')
    if (res) {
      let parse = JSON.parse(res)
      this.setState({
        empID: parse[0].empID
      },()=>console.log(this.state.empID))
    } else {
    }


  }


  _onlogin = async (event) => {
    if (event.key === 'Enter') {
      let password = this.state.password
      let confirm = this.state.confirm

      if (password !== confirm) {
        swal({
          title: "รหัสผ่านไม่ตรงกัน",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
      } else {



        let object = {
          email: this.state.email,
          password: this.state.password,
          // empID: this.state.empID
        };


        try {
          let res = await post("change_password", object)
          if (res.success == true) {
            swal("เปลี่ยนรหัสผ่านสำเร็จ")
            window.location.href="/login"
            console.log("result", res.result)


          } else {
            alert(JSON.stringify(res.message))
          }

        } catch (error) {
          alert(error);
        }
      }
    }

  };

  goto_register = () => {
    window.location.href = "/register"
  }


  render() {
    return (
      <div style={divStyle}>

        <div style={{ padding: 0, width: '100%' }}>
          <div style={{ width: 280, display: 'center', justifyContent: 'center', float: 'center' }}>


            <img src={Banner} width="100%" style={{ cursor: 'pointer' }} />

            <div style={{ fontSize: 30, color: '#4dd2ff' }}>
              LOGIN
            </div>

            <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} style={{ marginTop: 20 }} placeholder="Username (Office Email)" onChange={(event) => this.setState({ email: event.target.value })} />
            <Input.Password prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} style={{ marginTop: 20 }} placeholder="Password" onChange={(event) => this.setState({ password: event.target.value })} />
            <Input onKeyPress={(event)=>this._onlogin(event)} style={{ marginTop: 20 }} placeholder="Confirm Password" onChange={(event) => this.setState({ confirm: event.target.value })} />


            <div style={{
              width: 280, marginTop: 40, marginBottom: 200
            }}

              onClick={() => { this._onlogin() }} className='default_button' bsSize="large">Login
                </div>



          </div>
        </div>
      </div>
    );
  }
}
export default register;
