import React, { Component, useState } from 'react'
import Banner from '../../asset/logo/logo.png'
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import { Input ,Icon} from 'antd';
import pjs from './pjss.png';
import TextField from '@material-ui/core/TextField';
import { post } from '../../service/service'
import swal from 'sweetalert'


const styles = {
  margin: 10,
  width: 60,
  height: 60
  // margin: theme.spacing.unit,
  // backgroundColor: theme.palette.secondary.main,
};
const divStyle = {
  textAlign: "-webkit-center"
};

class login extends Component {
  constructor() {
    super();
    this.state = {
      username: null,
      password: null
    };
  }
  oninput = event => {
    this.setState({ [event.target.name]: event.target.value });
  };
  _onlogin = async (event) => {

    if (event.key === 'Enter') {
      console.log('enter key pressed')
      let object = {
        username: this.state.username,
        password: this.state.password
      };
     
     
      try {
        let res = await post("login_web",object)
     
        if (res.success == true) {
      
            localStorage.setItem('data',JSON.stringify(res.result));
            swal({
              title: "เข้าสู่ระบบสำเร็จ",
             
              icon: "success",
            });
            setTimeout(()=>{
              window.location.href = "/" ;
            },1000)
           
          console.log("result",res.result)
    
          
        } else {
          alert(JSON.stringify(res.message))
        }
  
      } catch (error) {
        alert(error);
      }
    }
   
  };

  goto_register = ()=>{
    window.location.href="/register"
  }

  render() {
    return (
      <div style={divStyle}>

        <div style={{ padding: 0, width: '100%' }}>
          <div style={{ width: 280, display: 'center', justifyContent: 'center', float: 'center' }}>


            <img src={Banner} width="100%" style={{ cursor: 'pointer' }} />

            <div style={{ fontSize: 30, color: '#4dd2ff' }}>
              LOGIN
            </div>

            <Input   prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} style={{ marginTop: 20 }} placeholder="Username" onChange={(event) => this.setState({ username: event.target.value })} />
            <Input.Password   onKeyPress={(event) =>  this._onlogin(event) } prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} style={{ marginTop: 20 }} placeholder="Password" onChange={(event) => this.setState({ password: event.target.value })} />


            <div style={{ width: 280, marginTop: 40}}
            
            onClick={(event) =>  this._onlogin(event) } className='default_button' bsSize="large">Login

            
                </div>

            <div onClick={()=>this.goto_register()} style={{ fontSize: 15, color: '#4dd2ff', marginTop: 20, marginBottom: 300,cursor:'pointer' }}>
              Forgot your Password ?
            </div>
           
          </div>
        </div>
      </div>
    );
  }
}
export default login;
