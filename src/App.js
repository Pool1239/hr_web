import React, { Component } from 'react';
import './App.css';
import RouterChild from '../src/router/router'
import { BrowserRouter as Router, Link, Route } from 'react-router-dom'
import Banner from '../src/asset/logo/logo.png'

import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import { Nav, NavItem, Dropdown, DropdownItem, DropdownToggle, DropdownMenu, NavLink } from 'reactstrap';
import { Layout, Avatar } from 'antd';
import { get, post } from './service/service';


class App extends Component {
  state = {
    anchorEl: null,
    toggle: this.toggle.bind(this),
    state: {
      dropdownOpen: false,
      empID: 0,
      role: ""

    }
  };



  toggle() {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen
    });
  }

  _onlogout = () => {
    localStorage.removeItem('data')
    localStorage.removeItem('private')
    window.location.href = '/'
  }

  componentWillMount = async () => {

    let reses = localStorage.getItem('data')
    let parse = JSON.parse(reses)

    try {
      let reses = await post('/private', { empID: parse[0].empID })
      console.log("หน้าhome", reses.result)
      localStorage.setItem('private', JSON.stringify(reses.result));
    } catch (error) {
      alert(error)
    }
    setTimeout(() => {
      let ress = localStorage.getItem('private')
      if (ress) {
        console.log("res", JSON.parse(ress))
        let parse = JSON.parse(ress)
        this.setState({
          empID: parse[0].empID,
          role: parse[0].role,
          private: parse[0]
        }, () => console.log("ข้อมูลส่วนตัว", this.state.private))
      }
    }, 100)




  }





  render() {
    const {
      Header, Footer, Sider, Content,
    } = Layout;
    const { empID, role } = this.state;
    return (
      <Router>
        <div >
          <div style={{ cursor: 'pointer' }}>
            <div style={{ display: 'flex', justifyContent: 'space-between', backgroundColor: '#33ccff' }}>

              <Nav tabs>
                {
                  role === 2 || role === 1 || role === 3 ?

                    <NavLink active><div style={{ width: 100, height: 40 }}><img src={Banner} width="100%" /></div></NavLink>
                    : null
                }

                {
                  role === 2 || role === 1 || role === 3 ?

                    <NavLink href="/">Home</NavLink>

                    : null
                }

                {
                  role === 1 || role === 3 ?

                    <NavLink href="/leaverequestadmin">LeaveRequest</NavLink>
                    : null
                }
                {
                  role === 1 || role === 3 ?

                    <NavLink href="/user">User</NavLink>
                    : null
                }
                {
                  role === 2 ?

                    <NavLink href="/leaverequest_remaining">leaverequest_remaining</NavLink>
                    : null
                }

                {
                  role === 1 || role === 3 ?
                    <Dropdown nav isOpen={this.state.dropdownOpen} toggle={() => this.toggle()}>
                      <DropdownToggle nav caret>
                        Report
                    </DropdownToggle>
                      <DropdownMenu>

                        <DropdownItem href='/leavereport'>Leave Report</DropdownItem>
                        <DropdownItem href='/flexiblebenefitreport' >Flexible Benefit Report</DropdownItem>

                      </DropdownMenu>
                    </Dropdown>
                    : null
                }
                {
                  role === 1 || role === 3 ?

                    <NavLink href='/myleaverequest'>My Leave Request</NavLink>

                    : null
                }
                {
                  role === 2 ?

                    <NavLink href='/testdatatable'>testdatatable</NavLink>

                    : null
                }


              </Nav>

              {
                role ?
                  <div style={{ margin: 'auto', backgroundColor: 'transparent' }} class="dropdown">
                    <div style={{ color: '#ffffff', display: 'flex', marginRight: 20, margin: 'auto', width: 200 }}>
                      <Avatar style={{ margin: 'auto' }} size="large" icon="user" />
                      <div style={{ marginRight: 30 }}>
                        <p>{this.state.private.userName}</p>
                        <p>{this.state.private.full_name}</p>
                      </div>

                    </div>

                    <div class="dropdown-content" >


                      <NavLink href="/login" onClick={() => this._onlogout()} >  {empID ? "Logout" : "Login"} </NavLink>

                    </div>
                  </div>

                  : <NavLink href="/login" onClick={() => this._onlogout()} >  {empID ? "Logout" : "Login"} </NavLink>
              }
            </div>

          </div>

          <div>
            <RouterChild />
          </div>
          <Layout>

            <Footer style={{ background: '#4dd2ff', textAlign: 'center', color: '#ffffff', fontSize: 20, lineHeight: 'normal' }}>Copyright OFFICE ONE (Thialand) Co.Ltd</Footer>
          </Layout>
          {/* <div style={{ textAlign: 'center' ,color:'#ffffff'}}>
            <Footer copyrights="Copyright OFFICE ONE (Thialand) Co.Ltd"


            >

            </Footer>

          </div> */}
        </div>
      </Router>
    );
  }
}

export default App;
